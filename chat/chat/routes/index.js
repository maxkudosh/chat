﻿'use strict';

var express = require('express');
var router = express.Router();

var passport = require('passport');

router.get('/', function (req, res) {
    res.render('index');
});

router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});

router.post('/', passport.authenticate('local', {
    successRedirect: '/chat',
    failureRedirect: '/',
    failureFlash: true
}));

module.exports = router;