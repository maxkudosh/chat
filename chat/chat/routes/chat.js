﻿'use strict';

var express = require('express');
var router = express.Router();

var isAuthenticated = function (req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
};

router.get('/', isAuthenticated, function (req, res, next) {
    res.render('chat', req.user);
});

module.exports = router;