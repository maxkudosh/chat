﻿define(['io', 'dispatcher', 'events', 'entities/user', 'entities/message'], function (Io, eventDispatcher, Events, User, Message) {
    
    'use strict';

    var socket;

    function ChatSocket(user) {

        this.socket = new Io({
            query: {
                username: user.name
            }
        });

        this.socket.on(Events.CHAT_LOADED, function (data) {

            eventDispatcher.fire(Events.CHAT_LOADED, data);

        });

        this.socket.on(Events.PRIVATE_CHAT_LOADED, function (data) {
            
            eventDispatcher.fire(Events.PRIVATE_CHAT_LOADED, data);

        });

        this.socket.on(Events.USER_JOINED, function (name) {
            
            eventDispatcher.fire(Events.USER_JOINED, name);

        });

        this.socket.on(Events.USER_LEFT, function (name) {
            
            eventDispatcher.fire(Events.USER_LEFT, name);

        });

        this.socket.on(Events.INCOMING_MESSAGE, function (message) {
            
            eventDispatcher.fire(Events.INCOMING_MESSAGE, message);

        });
        
        eventDispatcher.on(Events.PRIVATE_CHAT_INITIATED, function (recipient) {

            this.socket.emit(Events.PRIVATE_CHAT_INITIATED, { author: user.name, recipient: recipient });

        }, this);

        eventDispatcher.on(Events.OUTCOMING_MESSAGE, function (message, recipient) {

            var newMessage = new Message(user.name, recipient, message, new Date());

            this.socket.emit(Events.INCOMING_MESSAGE, newMessage);
            eventDispatcher.fire(Events.INCOMING_MESSAGE, newMessage);

        }, this);

    }
    
    ChatSocket.prototype.disconnect = function () {
        
        this.socket.disconnect();

    };
    
    var singleSocket;
    
    return {
        
        connect: function (user) {
            if (!singleSocket) {
                singleSocket = new ChatSocket(user);
            }
        },
        
        disconnect: function () {
            if (singleSocket) {
                singleSocket.disconnect();
            }
        }

    };

});