﻿define([], function () {
    
    'use strict';

    function Message(author, recipient, text, date) {

        this.author = author;
        this.recipient = recipient;
        this.text = text;
        this.date = new Date(date);

    }

    return Message;

});