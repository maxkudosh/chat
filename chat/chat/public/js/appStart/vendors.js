﻿define(['jquery', 'bootstrap', 'text', 'knockout', 'knockout-amd-helpers', 'io', 'toastr'], function ($, b, text, ko, koh, io, toastr) {
    
    'use strict';

    function init() {

        ko.amdTemplateEngine.defaultPath = '../templates';

    }

    return {
        init: init
    };
});
