﻿define(['events', 'dispatcher'], function (Events, dispatcher) {
    
    'use strict';

    function isPrivateMessage(message) {

        return message.recipient;

    }

    function initNotifications(currentUserName){

        dispatcher.on(Events.INCOMING_MESSAGE, function (message) {

            if (message.author === currentUserName) {
                return;
            }

            var title = isPrivateMessage(message) ? 'New message from ' + message.author : 'New message in chat';

            var messageNotification = new Notification(title, { body: message.text });
            
            if (isPrivateMessage(message)) {

                messageNotification.addEventListener('click', function () {
                    dispatcher.fire(Events.PRIVATE_CHAT_INITIATED, message.author);
                    window.focus();
                });

            } 
            
            messageNotification.addEventListener('click', function () {
                window.focus();
                messageNotification.close();
            });


        });

    }

    function init(currentUsername){

        if (!("Notification" in window)) {
            return;
        }
        
        if (Notification.permission === "granted") {

            initNotifications(currentUsername);

        }
        else if (Notification.permission !== 'denied') {

            Notification.requestPermission(function (permission) {

                if (permission === "granted") {
                    initNotifications(currentUsername);
                }
            });

        }

    }

    return {

        init: init

    };

});