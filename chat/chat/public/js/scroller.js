﻿define(['jquery'], function ($) {
    
    'use strict';

    var config = {
        childList: true,
        subtree: true
    };

    function observeSelector(selector){

        var target = document.querySelector(selector);

        if (!target) {
            return;
        }

        var observer = new MutationObserver(function () {
            var chatToScroll = $(selector);
            chatToScroll.scrollTop(chatToScroll[0].scrollHeight);
        });

        observer.observe(target, config);

    }

    function init(){

        observeSelector('#private .chat');
        observeSelector('#common .chat');

    }

    return {
        
        init: init

    };

});