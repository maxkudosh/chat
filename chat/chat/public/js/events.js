﻿define(function () {
    
    'use strict';

    var Events = {
        
        CHAT_LOADED: 'chat loaded',
        PRIVATE_CHAT_INITIATED: 'private chat initiated',
        PRIVATE_CHAT_LOADED: 'private chat loaded',
        USER_JOINED: 'user joined',
        USER_LEFT: 'user left',
        INCOMING_MESSAGE: 'incoming message',
        OUTCOMING_MESSAGE: 'outcoming message'

    };

    return Events;

});