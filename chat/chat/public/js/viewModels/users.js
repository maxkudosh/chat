﻿define([
    'knockout',
    'utilities',
    'events',
    'dispatcher',
    './baseViewModel',
    'entities/user'
], function (ko, utilities, Events, eventDispatcher, BaseViewModel, User) {
    
    'use strict';
    
    function UsersViewModel(username) {
        
        UsersViewModel.superclass.constructor.call(this, '#users', 'users');
        
        this.username = username;
        
        this.users = ko.observableArray();
        
        eventDispatcher.on(Events.CHAT_LOADED, function (data) {
            
            this.users(data.usernames.map(createUser));

        }, this);
        
        eventDispatcher.on(Events.USER_JOINED, function (name) {
            
            this.users.push(createUser(name));

        }, this);
        
        eventDispatcher.on(Events.USER_LEFT, function (name) {
            
            var usersOnline = this.users().filter(function (user) { return user.name != name; });
            
            this.users(usersOnline);

        }, this);

    }
    
    utilities.inherit(UsersViewModel, BaseViewModel);
    
    UsersViewModel.prototype.userClicked = function (user) {
        
        if (user.name == this.username) {
            return;
        }
        
        eventDispatcher.fire(Events.PRIVATE_CHAT_INITIATED, user.name);

    };
    
    function createUser(name) {
        return new User(name);
    }
    
    return UsersViewModel;

});