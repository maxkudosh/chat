﻿define(['knockout'], function (ko) {
    
    'use strict';

    function BaseViewModel(container, template) {

        this.container = container;
        this.template = template;

        return this;
    }

    BaseViewModel.prototype.bind = function () {
        var container = document.querySelector(this.container);
        ko.applyBindings(this, container);
    };

    return BaseViewModel;

});