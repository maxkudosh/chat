﻿define([
    'knockout',
    'utilities',
    'dispatcher',
    'events',
    './baseViewModel',
    './childViewModels/chat',
    './childViewModels/message'
], function (ko, utilities, dispatcher, Events, BaseViewModel, ChatViewModel, MessageViewModel) {
    
    'use strict';

    function PrivateChatViewModel(recipient) {

        PrivateChatViewModel.superclass.constructor.call(this, '#private', 'private');

        this.recipient = ko.observable(recipient);

        this.chat = new ChatViewModel('#private .chat', 'chat', recipient);
        this.message = new MessageViewModel('#private .message', 'message', recipient);

        dispatcher.on(Events.PRIVATE_CHAT_LOADED, function (data) {

            this.recipient(data.recipient);
            this.message.recipient(data.recipient);
            this.chat.recipient(data.recipient);
            this.chat.load(data.messages);

        }, this);

        dispatcher.on(Events.INCOMING_MESSAGE, function (message) {

            if (message.recipient !== null) {
                this.chat.push(message);
            }

        }, this);

    }
    
    utilities.inherit(PrivateChatViewModel, BaseViewModel);
    
    return PrivateChatViewModel;

});