﻿define([
    'knockout', 
    'utilities', 
    'dispatcher', 
    'events', 
    './baseViewModel', 
    './childViewModels/chat', 
    './childViewModels/message'
], function (ko, utilities, dispatcher, Events, BaseViewModel, ChatViewModel, MessageViewModel) {
    
    'use strict';
    
    function CommonChatViewModel() {
        
        CommonChatViewModel.superclass.constructor.call(this, '#common', 'common');
        
        var commonChatRecipient = null;
        
        this.chat = new ChatViewModel('#common .chat', 'chat', commonChatRecipient);
        this.message = new MessageViewModel('#common .message', 'message', commonChatRecipient);
        
        dispatcher.on(Events.CHAT_LOADED, function (data) {
            
            this.chat.load(data.messages);

        }, this);
        
        dispatcher.on(Events.INCOMING_MESSAGE, function (message) {
            
            if (message.recipient === null) {
                this.chat.push(message);
            }

        }, this);

    }
    
    utilities.inherit(CommonChatViewModel, BaseViewModel);
    
    return CommonChatViewModel;

});