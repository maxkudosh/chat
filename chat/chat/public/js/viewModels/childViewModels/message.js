﻿define([
    'knockout',
    'utilities',
    'events',
    'dispatcher',
    '../baseViewModel',
    'entities/message'
], function (ko, utilities, Events, eventDispatcher, BaseViewModel, Message) {
    
    'use strict';
    
    function MessageViewModel(container, template, recipient) {
        
        MessageViewModel.superclass.constructor.call(this, container, template);
        
        this.text = ko.observable();
        this.recipient = ko.observable(recipient);

    }
    
    utilities.inherit(MessageViewModel, BaseViewModel);
    
    MessageViewModel.prototype.send = function () {
        
        eventDispatcher.fire(Events.OUTCOMING_MESSAGE, this.text(), this.recipient());
        this.text('');

    };
    
    return MessageViewModel;

});