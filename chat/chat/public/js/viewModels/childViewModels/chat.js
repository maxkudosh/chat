﻿define([
    'knockout',
    'utilities',
    'events',
    'dispatcher',
    '../baseViewModel',
    'entities/message'
], function (ko, utilities, Events, eventDispatcher, BaseViewModel, Message) {
    
    'use strict';

    function ChatViewModel(container, template, recipient) {

        ChatViewModel.superclass.constructor.call(this, container, template);

        this.recipient = ko.observable(recipient);
        this.author = ko.observable();

        this.messages = ko.observableArray();

    }

    utilities.inherit(ChatViewModel, BaseViewModel);

    ChatViewModel.prototype.load = function (messages) {

        this.messages(messages.map(mapMessage));

    };
    
    ChatViewModel.prototype.push = function (message) {
        
        this.messages.push(mapMessage(message));

    };

    function mapMessage(jsonMessage) {
        return new Message(jsonMessage.author, jsonMessage.recipient, jsonMessage.text, jsonMessage.date);
    }

    return ChatViewModel;

});