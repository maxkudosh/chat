﻿'use strict';

require.config({
    baseUrl: 'js',
    shim: {
        'bootstrap': {
            deps: ['jquery']
        }
    },
    paths: {
        'jquery': '../vendors/jquery/dist/jquery.min',
        'bootstrap': '../vendors/bootstrap/dist/js/bootstrap.min',
        'knockout': '../vendors/knockout/dist/knockout',
        'knockout-amd-helpers': '../vendors/knockout-amd-helpers/build/knockout-amd-helpers.min',
        'text': '../vendors/requirejs-text/text',
        'io': '../vendors/socket.io-client/socket.io',
        'toastr': '../vendors/toastr/toastr.min'
    }
});

require([
    'appStart/vendors',
    'socket',
    'scroller',
    'notifications',
    'jquery',
    'viewModels/users',
    'dispatcher',
    'events',
    'viewModels/commonRoom',
    'viewModels/privateRoom',
    'entities/user'], function (vendors, socket, scroller, notifications, $, Users, eventDispatcher, Events, CommonRoom, PrivateRoom, User) {

    vendors.init();

    var user = new User($('#username').val());

    var usersViewModel = new Users(user.name);
    usersViewModel.bind();

    var commonChat = new CommonRoom();
    commonChat.bind();

    var privateChat = new PrivateRoom();
    privateChat.bind();

    notifications.init(user.name);

    eventDispatcher.on(Events.PRIVATE_CHAT_INITIATED, function (recipientName) {

        if (user.name != recipientName) {
            $('#private').modal();
        }

    });

    eventDispatcher.on(Events.CHAT_LOADED, scroller.init);

    socket.connect(user);

    $('body').bind('beforeunload', function () {
        socket.disconnect();
    });

});