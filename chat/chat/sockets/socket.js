﻿'use strict';

var io = require('socket.io');
var sanitizer = require('../private/sanitizer');
var MessageService = require('../private/services/messageService');

function ActiveUser(username, socketIds) {
    this.username = username;
    this.socketIds = socketIds;
}

function Socket(server){

    var connectedUsers = [];
    var messages = new MessageService();

    var sockets = io(server);

    sockets.on('connection', function (socket) {

        var username = sanitizer.sanitizeString(socket.handshake.query.username);

        var existingUser = findUserByName(username, connectedUsers);

        if (!existingUser) {
            socket.broadcast.emit('user joined', username);
            var newUser = new ActiveUser(username, [socket.id]);
            connectedUsers.push(newUser);
        } else {
            existingUser.socketIds.push(socket.id);
        }

        socket.on('disconnect', function () {

            connectedUsers = connectedUsers.filter(function (user) {

                user.socketIds = user.socketIds.filter(function (socketId) {
                    return socketId != socket.id;
                });

                return user.socketIds && user.socketIds.length > 0;

            });

            sockets.emit('user left', username);

        });

        messages.getCommon().then(function (commonMessages) {

            socket.emit('chat loaded', {
                usernames: connectedUsers.map(function(user) { return user.username; }),
                messages: commonMessages
            });

        });

        socket.on('incoming message', function(newMessage) {
            
            var sanitizedNewMessage = sanitizer.sanitizeMessage(newMessage);

            messages.create(sanitizedNewMessage).then(function () {

                if (sanitizedNewMessage.recipient) {

                    var recipient = findUserByName(sanitizedNewMessage.recipient, connectedUsers);

                    if (!recipient) {
                        return;
                    } 

                    recipient.socketIds.forEach(function (socketId) {

                        sockets.to(socketId).emit('incoming message', sanitizedNewMessage);

                    });

                } else {

                    socket.broadcast.emit('incoming message', sanitizedNewMessage);

                }

            });

        });

        socket.on('private chat initiated', function(data) {
            
            var privateChatAuthor = sanitizer.sanitizeString(data.author);
            var privateChatRecipient = sanitizer.sanitizeString(data.recipient);

            messages.getConversation(privateChatAuthor, privateChatRecipient).then(function(conversation) {

                socket.emit('private chat loaded', { author: privateChatAuthor, recipient: privateChatRecipient, messages: conversation });

            });

        });

    });

}

function findUserByName(name, users) {

    return users.filter(function (user) {
        return user.username == name;
    })[0];

}


module.exports = Socket;