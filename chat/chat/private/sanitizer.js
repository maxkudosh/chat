﻿'use strict';

var sanitizer = require('sanitizer');

function sanitizeString(s){

    return sanitizer.sanitize(s);

}

function sanitizeMessage(message) {

    message.text = sanitizeString(message.text);

    if (message.date instanceof String) {
        message.date = sanitizeString(message.date);
    }

    message.username = sanitizeString(message.username);

    return message;

}

function sanitizeUser(user){

    user.username = sanitizeString(user.username);
    user.password = sanitizeString(user.password);

    return user;

}

module.exports = {
    
    sanitizeString: sanitizeString,
    sanitizeMessage: sanitizeMessage,
    sanitizeUser: sanitizeUser

};