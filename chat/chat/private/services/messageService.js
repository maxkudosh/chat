﻿'use strict';

var dal = require('../dal/dal');
var Message = require('../entities/message');

function MessageService() {

}

MessageService.prototype.create = function (newMessage) {

    return dal.messages.create(newMessage);

};

MessageService.prototype.getCommon = function () {

    return dal.messages.find({ recipient: null }, { date: 1 });

};

MessageService.prototype.getConversation = function (first, second) {

    return dal.messages.find(getPrivateMessageCriteria(first, second), { date: 1 });

};

function getPrivateMessageCriteria(fisrt, second) {
    return {
        $or: [
            { author: fisrt, recipient: second },
            { author: second, recipient: fisrt }
        ]
    };
}

module.exports = MessageService;