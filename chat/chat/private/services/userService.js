﻿'use strict';

var dal = require('../dal/dal');
var User = require('../entities/user');

function UserService() {

}

UserService.prototype.create = function (user) {

    return dal.users.create(user);

};

UserService.prototype.getByName = function (username) {

    return dal.users.findOne({ username: username });

};

UserService.prototype.getById = function (id) {

    return dal.users.findOne({ _id: id });

};

module.exports = UserService;