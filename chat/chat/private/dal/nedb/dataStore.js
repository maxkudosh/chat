﻿'use strict';

var DataStore = require('nedb');

var dataStores = {};

module.exports = {

    getDataStore: function (type) {

        if (!dataStores[type]) {
            dataStores[type] = new DataStore({ filename: 'nedb/' + type, autoload: true });
            
        }

        return dataStores[type];

    }

};