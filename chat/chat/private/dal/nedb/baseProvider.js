﻿'use strict';

var dsBuilder = require('./dataStore');
var User = require('../../entities/user');

function BaseProvider(dataStoreName) {

    this.ds = dsBuilder.getDataStore(dataStoreName);

}

function createDbOperationCallback(resolve, reject, resultFormatter) {

    return function (error, operationResult) {
        
        if (error) {
            reject(error);
            return;
        }
        
        if (resultFormatter) {
            operationResult = resultFormatter(operationResult);
        }
        
        resolve(operationResult);

    };

}

BaseProvider.prototype.findOne = function (query, resultFormatter) {

    var self = this;

    return new Promise(function (resolve, reject) {

        self.ds.findOne(query, createDbOperationCallback(resolve, reject, resultFormatter));

    });

};

BaseProvider.prototype.find = function (query, resultFormatter, sort) {

    var self = this;

    return new Promise(function (resolve, reject) {
        
        if (sort) {
            self.ds.find(query).sort(sort).exec(createDbOperationCallback(resolve, reject, resultFormatter));
        } else {
            self.ds.find(query, createDbOperationCallback(resolve, reject, resultFormatter));
        }
        

    });

};

BaseProvider.prototype.create = function (newItem) {

    var self = this;

    return new Promise(function (resolve, reject) {

        self.ds.insert(newItem, createDbOperationCallback(resolve, reject));

    });

};

module.exports = BaseProvider;