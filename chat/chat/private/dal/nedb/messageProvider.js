﻿'use strict';

var Provider = require('./baseProvider');
var Message = require('../../entities/message');

function MessageProvider() {

    this.provider = new Provider('messages');

}


MessageProvider.prototype.find = function (query, sort) {

    return this.provider.find(query, Message.fromDbUser, sort);

};

MessageProvider.prototype.findOne = function (query) {

    return this.provider.findOne(query, Message.fromDbUser);

};

MessageProvider.prototype.create = function (message) {

    return this.provider.create(message);

};

module.exports = MessageProvider;