﻿'use strict';

var Provider = require('./baseProvider');
var User = require('../../entities/user');

function UserProvider() {

    this.provider = new Provider('users');

}


UserProvider.prototype.findOne = function (query) {

    return this.provider.findOne(query, User.fromDbUser);

};

UserProvider.prototype.create = function (newUser) {

    return this.provider.create(newUser);

};

module.exports = UserProvider;