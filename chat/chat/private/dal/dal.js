﻿'use strict';

var UserDataProvider = require('./nedb/userProvider');
var MessageDataProvider = require('./nedb/messageProvider');

var dal =  {

    users: new UserDataProvider(),
    messages: new MessageDataProvider()

};

module.exports = dal;