﻿'use strict';

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var User = require('./entities/user');
var UserService = require('../private/services/userService');

var userService = new UserService();

function auth(username, password, done) {

    var userPromise = userService.getByName(username);

    userPromise.then(function(user) {

        if (!user) {
            var newUserPromise = userService.create(new User(username, password));
            newUserPromise.then(function(newUser) {
                return done(null, newUser);
            });
            newUserPromise.catch(function() {
                return done(null, false, 'Fail! Try again.');
            });
        }

        if (user.username == username && user.password == password) {
            return done(null, user);
        }

        return done(null, false, 'Wrong password!');

    });

    userPromise.catch(function () {
        return done(null, false, 'Fail! Try again.');
    });

}

passport.serializeUser(function (user, done) {
    done(null, user._id);
});

passport.deserializeUser(function (id, done) {

    var userPromise = userService.getById(id);

    userPromise.then(function (user) {
        done(null, user);
    });

    userPromise.catch(function (error) {
        done(error);
    });

});

module.exports.init = function () {

    passport.use(new LocalStrategy(auth));

};