﻿'use strict';

function User(username, password){

    this.username = username;
    this.password = password;

}

User.fromDbUser = function (dbUser) {
    
    if (!dbUser) {
        return;
    }
    
    var user = new User(dbUser.username, dbUser.password);
    
    user._id = dbUser._id;
    
    return user;

};

module.exports = User;