﻿'use strict';

function Message(author, recipient, text, date) {

    this.author = author;
    
    if (recipient) {
        this.recipient = recipient;
    }
    else {
        this.recipient = null;
    }

    this.date = new Date(date);
    this.text = text;

}

Message.fromDbMessage = function (dbMessage) {
    
    if (!dbMessage) {
        return;
    }
    
    var message = new Message(dbMessage.author, dbMessage.recipient, dbMessage.text, dbMessage.date);
    
    message._id = dbMessage._id;
    
    return message;

};

module.exports = Message;