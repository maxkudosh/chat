﻿'use strict';

module.exports = function (grunt) {

    grunt.initConfig({
        jshint: {
            files: ['Gruntfile.js', 'app.js', 'bin/www', 'private/**/*.js', 'public/js/**/*.js'],
            options: {
                globals: {
                    require: true,
                    define: true,
                    module: true,
                    Promise: true
                },
                node: true
            }
        },
        watch: {
            files: ['<%= jshint.files %>'],
            tasks: ['jshint']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['jshint']);

};